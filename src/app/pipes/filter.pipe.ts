import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'filter',
})
export class FilterPipe implements PipeTransform {
  transform([...users]: any, active: boolean = null): unknown {
    console.log(active);
    return active === null
      ? users
      : users.filter((user) => user.status === active);
  }
}
