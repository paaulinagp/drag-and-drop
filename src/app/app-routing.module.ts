import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { DrapAndDropComponent } from './components/drap-and-drop/drap-and-drop.component';
import { ListComponent } from './pages/list/list.component';

const routes: Routes = [
  { path: 'drag-drop', component: DrapAndDropComponent },
  { path: 'list', component: ListComponent },
  { path: '**', pathMatch: 'full', redirectTo: 'list' },
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
