import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-list',
  templateUrl: './list.component.html',
  styleUrls: ['./list.component.css'],
})
export class ListComponent implements OnInit {
  orderAsc: boolean;
  filterStatus: boolean | null;

  lista: any = [
    { name: 'Lalo', status: true },
    { name: 'Claudia', status: false },
    { name: 'Raphael', status: true },
    { name: 'Paulina', status: false },
  ];

  constructor() {
    this.orderAsc = true;
  }

  ngOnInit(): void {}

  cambiarFiltro(event): void {
    const filtro = event.target.value;
    switch (filtro) {
      case 'activados':
        this.filterStatus = true;
        break;
      case 'desactivados':
        this.filterStatus = false;
        break;
      default:
        this.filterStatus = null;
        break;
    }
  }
}
